const electron = require('electron');
const path = require('path');
const url = require('url');

const {app, BrowserWindow, BrowserView, Menu} = electron;
const ipc = electron.ipcMain

let mainWindow;

app.on('ready', function(){
    // Create new window
    mainWindow = new BrowserWindow({});
    // Load html in window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'templates/main.html'),
        protocol: 'file:',
        slashes:true
    }));

});

ipc.on('loginPage', function(event) {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'templates/login.html'),
        protocol: 'file:',
        slashes:true
    }));
});

ipc.on('signupPage', function(event) {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'templates/signup.html'),
        protocol: 'file:',
        slashes:true
    }));
})


//menu template

const mainMenuTemplate =[
    {
        label:'File'
    }
];
