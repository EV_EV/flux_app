const electron = require('electron')
const ipc = electron.ipcRenderer

const loginButton = document.getElementById('login-button')
const signupButton = document.getElementById('signup-button')

loginButton.addEventListener('click', function() {
    ipc.send('loginPage')
})

signupButton.addEventListener('click', function() {
    ipc.send('signupPage')
})
