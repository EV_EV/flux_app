const electron = require('electron')
const ipc = electron.ipcRenderer

const loginButton = document.getElementById('submit')
const usernameField = document.getElementById('username')
const passwordField = document.getElementById('password')

loginButton.addEventListener('click', function() {
    const un = usernameField.value
    const pw = passwordField.value
    console.log('Username '+un)
    console.log('Password '+pw)
})