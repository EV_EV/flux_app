const electron = require('electron')
const ipc = electron.ipcRenderer

const loginButton = document.getElementById('submit')
const usernameField = document.getElementById('username')
const passwordField = document.getElementById('password')
const emailField = document.getElementById('email')

loginButton.addEventListener('click', function() {
    const un = usernameField.value
    const pw = passwordField.value
    const email = emailField.value
    console.log('Username '+un)
    console.log('Email '+email)
    console.log('Password '+pw)
})